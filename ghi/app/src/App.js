import Nav from './Nav.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js';
import AttendConferenceForm from './AttendConferenceForm.js';
import ConferenceForm from './ConferenceForm.js';
import PresentationForm from './PresentationForm.js';
import MainPage from './MainPage.js';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='locations'>
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />} />
          </Route>
          <Route path='attendees'>
          <Route path='' element={<AttendeesList />} ></Route>
            <Route path='new' element={<AttendConferenceForm />} />
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
